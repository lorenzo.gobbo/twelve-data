$(document).ready(function() {

    const API_KEY = '42d6e4b8371349dead68d83643cb08be';
    const INTERVAL = '1day';
    const SYMBOLS = ['ADBE', 'AAPL', 'BIDU', 'BKNG', 'EBAY', 'FB', 'EA', 'INTC', 'MSFT', 'NFLX', 'NVDA', 'PYPL', 'QCOM', 'MRNA', 'SBUX', 'TSLA', 'ZM', 'GOOG', 'GEVO', 'WIMI', 'NIO', 'XPEV', 'MA', 'XOM', 'AMD', 'BABA', 'AAL', 'KO', 'PEP', 'LMT', 'AXP', 'T', 'ALV', 'BA', 'CVX', 'DAL', 'GE', 'GM', 'MRO', 'MGM', 'NOK', 'NCLH', 'ORCL', 'TWTR', 'DIS', 'WDC', 'PLTR', 'TRIP', 'IBM', 'EXPE', 'ATVI', 'CERN', 'CSCO', 'CMCSA', 'DISCA', 'JD', 'LBTYK', 'MCHP', 'STX', 'TXN', 'VOD', 'WDC', 'XLNX', 'BK', 'BKR', 'CCL', 'C', 'DVN', 'DOW', 'F', 'FCX', 'GPS', 'GIS', 'HST', 'HPQ', 'KEY', 'KR', 'M', 'MRO', 'MU', 'MS', 'MOS', 'NEM', 'NEE', 'OXY', 'PFE', 'PSX', 'RCL', 'SLB', 'WFC', 'VIAC', 'V', 'VZ', 'FTI', 'YQ', 'AMTX', 'AFMD', 'ABNB', 'AESE', 'AMRN', 'POWW', 'ASTC', 'ARVL', 'APHA', 'BLU', 'BXRX', 'BLDP', 'BNGO', 'BIOL', 'CELC', 'CAN', 'CSCW', 'CIDM', 'CRIS', 'CRON', 'WISH', 'DKNG', 'ENDP'];
    const GROUP_LENGTH = 8;
    const SLEEP_TIME = 61000;
    const DATA_TABLE_PRECISION = 3;
    const HAMMER = {
        MARGIN: 0.1,
        RATIO: 0.33
    };
    const MORNING_STAR = {
        PERC: 0.02,
        BODY: 0.75
    };
    const PIERCING_LINE = {
        PERC: 0.02,
        BODY: 0.33
    };

    const PRECISION = 10 ** DATA_TABLE_PRECISION;
    let symbols_groups = [];
    let current_index = 0;
    let stocks = [];
    let fetch_interval;
    let progress_bar = document.querySelector('#progress-bar');
    let btn_load = document.querySelector('#btn-load');
    let ul_hamm = document.querySelector('#hammers');
    let ul_shoot_star = document.querySelector('#shooting-star');
    let ul_eng_bull = document.querySelector('#engulfing-bullish');
    let ul_eng_bear = document.querySelector('#engulfing-bearish');
    let ul_morn_star = document.querySelector('#morning-star');
    let ul_eve_star = document.querySelector('#evening-star');
    let ul_pier_line = document.querySelector('#piercing-line');
    let ul_dark_cloud = document.querySelector('#dark-cloud');

    let table = $('#stocks-table').DataTable({
        "createdRow": function(row, data, dataIndex) {
            if (data[2] > data[1]) {
                $(row).addClass('text-success');
            } else {
                $(row).addClass('text-danger');
            }
        }
    });

    for (let i = 0; i < Math.floor(SYMBOLS.length / GROUP_LENGTH) + 1; i++) {
        symbols_groups.push(SYMBOLS.slice(i * GROUP_LENGTH, (i + 1) * GROUP_LENGTH));
    }

    function api_url(i) {
        let str = 'https://api.twelvedata.com/time_series?symbol=';
        let sym_str = '';
        symbols_groups[i].forEach(stock => {
            sym_str += ',' + stock;
        });
        str += sym_str.substring(1) + '&interval=' + INTERVAL + '&apikey=' + API_KEY;
        return str
    }

    function fetchData() {
        axios.get(api_url(current_index)).then(resp => {
            saveData(resp.data);
            analyzeData();
            updatePage();
            let width = (current_index + 1) * GROUP_LENGTH * 100 / SYMBOLS.length;
            if (width >= 100) {
                width = 100;
                progress_bar.classList.add('bg-success');
            }
            progress_bar.style.width = width + '%';
            current_index++;
            if (current_index == symbols_groups.length) {
                current_index = 0;
                btn_load.disabled = false;
                clearInterval(fetch_interval);
            }
        }).catch(error => {
            console.log(error);
        });
    }

    btn_load.addEventListener('click', function() {
        ul_hamm.innerHTML = "";
        ul_eng_bull.innerHTML = "";
        //document.querySelector('#engulfing-bearish').innerHTML = "";
        progress_bar.classList.remove('bg-success');
        btn_load.disabled = true;
        fetchData();
        fetch_interval = setInterval(fetchData, SLEEP_TIME);
    });

    function saveData(resp_data) {
        if (!('status' in resp_data)) {
            Object.keys(resp_data).forEach(key => {
                stocks.push(resp_data[key]);
            });
        } else {
            stocks.push(resp_data);
        }
    }

    function updatePage() {
        let container = document.querySelector('#stocks-table-body');
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let perc;
                if (stock.values[0].open > stock.values[0].close) {
                    perc = -(stock.values[0].open - stock.values[0].close) / stock.values[0].open * 100;
                } else {
                    perc = (stock.values[0].close - stock.values[0].open) / stock.values[0].close * 100;
                }
                perc = Math.floor(perc * PRECISION) / PRECISION;
                table.row.add([
                    stock.meta.symbol,
                    Math.floor(stock.values[0].open * PRECISION) / PRECISION,
                    Math.floor(stock.values[0].close * PRECISION) / PRECISION,
                    Math.floor(stock.values[0].low * PRECISION) / PRECISION,
                    Math.floor(stock.values[0].high * PRECISION) / PRECISION,
                    perc,
                    stock.values[0].volume
                ]).draw(false);
            }
        };
    }

    function analyzeData() {
        hammers();
        shootingStar();
        engulfingBullish();
        engulfingBearish();
        morningStar();
        eveningStar();
        piercingLine();
        darkCloud();
    }

    function hammers() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let now = stock.values[0];
                let before = stock.values[1];
                if (now.volume > before.volume && now.low < before.low) {
                    if (now.close > now.high - (now.high - now.low) * HAMMER.MARGIN || now.open > now.high - (now.high - now.low) * HAMMER.MARGIN) {
                        if (Math.abs(now.open - now.close) <= HAMMER.RATIO * (now.high - now.low)) {
                            let li = document.createElement('li');
                            li.innerHTML = `<a target="_blank" href="https://it.investing.com/search/?q=${stock.meta.symbol}">${stock.meta.symbol}</a>`;
                            ul_hamm.appendChild(li);
                        }
                    }
                }
            }
        };
    }

    function shootingStar() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let now = stock.values[0];
                let before = stock.values[1];
                if (now.volume > before.volume && now.high > before.high) {
                    if ((now.high - now.low) * HAMMER.MARGIN > now.close - now.low || now.open - now.low < (now.high - now.low) * HAMMER.MARGIN) {
                        if (Math.abs(now.open - now.close) <= HAMMER.RATIO * (now.high - now.low)) {
                            let li = document.createElement('li');
                            li.innerHTML = `<a target="_blank" href="https://it.investing.com/search/?q=${stock.meta.symbol}">${stock.meta.symbol}</a>`;
                            ul_shoot_star.appendChild(li);
                        }
                    }
                }
            }
        };
    }

    function engulfingBullish() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let now = stock.values[0];
                let before = stock.values[1];
                if (now.close > now.open && now.low < before.low && now.open < Math.min(before.open, before.close) && now.close > Math.max(before.open, before.close) && now.high > before.high && now.volume > before.volume) {
                    let li = document.createElement('li');
                    li.innerHTML = `<a target="_blank" href="https://it.investing.com/search/?q=${stock.meta.symbol}">${stock.meta.symbol}</a>`;
                    ul_eng_bull.appendChild(li);
                }
            }
        };
    }

    function engulfingBearish() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let now = stock.values[0];
                let before = stock.values[1];
                if (now.close < now.open && now.low < before.low && now.close < Math.min(before.open, before.close) && now.open > Math.max(before.open, before.close) && now.high > before.high && now.volume > before.volume) {
                    let li = document.createElement('li');
                    li.innerHTML = `<a target="_blank" href="https://it.investing.com/search/?q=${stock.meta.symbol}">${stock.meta.symbol}</a>`;
                    ul_eng_bear.appendChild(li);
                }
            }
        };
    }

    function morningStar() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let now = stock.values[0];
                let yest = stock.values[1];
                let before = stock.values[2];
                if (before.open > before.close && (before.open - before.close) / before.open > MORNING_STAR.PERC && Math.max(yest.open, yest.close) < before.close && yest.low < before.low && yest.low < now.low && now.close > now.open && now.open > Math.min(yest.open, yest.close) && now.close - now.open > MORNING_STAR.BODY * (before.open - before.close) && now.volume > yest.volume) {
                    let li = document.createElement('li');
                    li.innerHTML = `<a target="_blank" href="https://it.investing.com/search/?q=${stock.meta.symbol}">${stock.meta.symbol}</a>`;
                    ul_morn_star.appendChild(li);
                }
            }
        };
    }

    function eveningStar() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let now = stock.values[0];
                let yest = stock.values[1];
                let before = stock.values[2];
                if (before.open < before.close && (before.close - before.open) / before.open > MORNING_STAR.PERC && Math.max(yest.open, yest.close) > before.close && yest.high > before.high && yest.high > now.high && now.close < now.open && now.open < Math.max(yest.open, yest.close) && now.open - now.close > MORNING_STAR.BODY * (before.close - before.open) && now.volume > yest.volume) {
                    let li = document.createElement('li');
                    li.innerHTML = `<a target="_blank" href="https://it.investing.com/search/?q=${stock.meta.symbol}">${stock.meta.symbol}</a>`;
                    ul_eve_star.appendChild(li);
                }
            }
        };
    }

    function piercingLine() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let now = stock.values[0];
                let before = stock.values[1];
                if (before.open > before.close && (before.open - before.close) / before.open > PIERCING_LINE.PERC && now.close > now.open && now.open < before.close && now.low < before.low && now.close > before.open - PIERCING_LINE.BODY * (before.open - before.close) && now.close < before.open && now.volume > before.volume) {
                    let li = document.createElement('li');
                    li.innerHTML = `<a target="_blank" href="https://it.investing.com/search/?q=${stock.meta.symbol}">${stock.meta.symbol}</a>`;
                    ul_pier_line.appendChild(li);
                }
            }
        };
    }

    function darkCloud() {
        for (let i = current_index * GROUP_LENGTH; i < stocks.length; i++) {
            let stock = stocks[i];
            if (stock.status == 'ok') {
                let now = stock.values[0];
                let before = stock.values[1];
                if (before.close > before.open && (before.close - before.open) / before.open > PIERCING_LINE.PERC && now.close < now.open && now.open > before.close && now.high > before.high && PIERCING_LINE.BODY * (before.open - before.close) > now.close - before.open && now.close > before.open && now.volume > before.volume) {
                    let li = document.createElement('li');
                    li.innerHTML = `<a target="_blank" href="https://it.investing.com/search/?q=${stock.meta.symbol}">${stock.meta.symbol}</a>`;
                    ul_dark_cloud.appendChild(li);
                }
            }
        };
    }

});